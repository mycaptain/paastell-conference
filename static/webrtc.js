//Configuration Variables
// const SIG_SERVER_URL = "https://deeprtc.ml:1403";
const SIG_SERVER_URL = "http://localhost:1403";

const TURN_SERVER_URL = 'deeprtc.ml:3478';
const TURN_SERVER_USERNAME = 'applepie';
const TURN_SERVER_CREDENTIAL = '140320';

const PEER_CONNECTION_CONFIG = {
    // iceServers: [
    //     {
    //         urls: 'turn:' + TURN_SERVER_URL + '?transport=tcp',
    //         username: TURN_SERVER_USERNAME,
    //         credential: TURN_SERVER_CREDENTIAL
    //     },
    //     {
    //         urls: 'turn:' + TURN_SERVER_URL + '?transport=udp',
    //         username: TURN_SERVER_USERNAME,
    //         credential: TURN_SERVER_CREDENTIAL
    //     }
    // ]
};


// const DEEP_ANALYSIS_URL = "https://deeprtc.ml:1999/dayeonie/"
const DEEP_ANALYSIS_URL = "http://localhost:1999/dayeonie/";

// Peer connections
let peercons = [];

// Stream found flag
let stream_stat = false

// Streams
let localStream; // Local stream we get from browser
let localAudioStream;
let localRecorder;
let localStreamElement = document.querySelector('#localStream'); // Video element to display local video source

// RecorderChuncks
let recordedChunks = [];

// Remote Streams
let remoteStreams = [];

// -------------------- Web Socket Client ------------------------
// Socket IO client, exposed as the namespace io with the standalone build (socket.io.js)
let socket = io(SIG_SERVER_URL, {
    autoConnect: false,
    query: {
        room: room_code
    }
});
//Client to communicate with the Python Socket.IO server
// autoConnect False -> wait until local media stream obtained

// The Python Socket.IO server emits two events : "data", "ready"(on connect)

// On 'data' event
socket.on('data', (data) => {
    handleSignalingData(data);  // Defined below
})  

// On 'ready' event given on 'connect'
socket.on('ready', (pc_idx) => {
    console.log('Ready');
    // Beyond two browsers(one peer connection), we need to add a peer connection per added brower
    // e.g 3 browsers require two peer connections per browser to connect all three, on and on
    createPeerConnection(Math.max(0, pc_idx - 1)); // the max lets us have 1 peer connection per browser until there are two browsers
    sendOffer(Math.max(0, pc_idx - 1));
})


// Method to broadcast data object to server with 'data' event, which the server will emit to all other clients
let sendData = (data) => {
    socket.emit('data', data);
}
// -----------------------------------------------------------

// Method to send POST Request to the Python Flask API server
async function getDeepResult(endpoint, senddata) {
    console.log(senddata)
    // Create form data object for multipart/form-data request
    let formdata = new FormData();
    // Add the audio blob with key 'audio' to the form-data
    var audioBlob = new Blob(recordedChunks, { type: 'audio/ogg; codecs=opus' });
    formdata.append('audio', audioBlob);

    // Using Fetch to send a POST request to the Python server
    let response = await fetch(DEEP_ANALYSIS_URL + endpoint, {
        method: 'POST',
        body: formdata,
    });
    remoteStreams = []
    let result = await response.json();
    return result;
}

// Main method : Get local video/audio device, stream and connect to websocket (events will get remote stream)
let getLocalStream = () => {
    navigator.mediaDevices.getUserMedia({
        audio:true,
        video:true
    }).then((stream) => {
        console.log(room_code);

        console.log('Stream Found');

        stream_stat = true;
        localStream = stream;
        localVideoStream = new MediaStream(localStream.getVideoTracks());
        localStreamElement.srcObject = localVideoStream;  // Display local stream on the browser
        
        // Create a separate mediastream that only carries the audio track
        localAudioStream = new MediaStream(localStream.getAudioTracks());
        
        // Instantiate a Recorder object that records audio in the webm format
        localRecorder = new MediaRecorder(localAudioStream);
        
        // Recorder event handler -> Will send data to Python api server once data blob is available
        localRecorder.ondataavailable = handleDataAvailable;
        
        // Start Recording
        localRecorder.start(1000);
        
        // Connect socket after local stream found and recording started
        socket.connect(); // -> Starts the connect event in the signalling server
    })
    .catch(error => {
        console.error('Stream not found: ', error);
    });
}

// Function to handle when recorder emits a data blob
function handleDataAvailable(event) {
    console.log("Data available!");
    if (event.data.size > 0) {
        if (recordedChunks.length == 0) {
            recordedChunks.push(event.data);   
        }

        recordedChunks.push(event.data)
        // Send the data to the python server for inference
        getDeepResult('sd', recordedChunks)
        .then(result => console.log(result));
        // }
        recordedChunks.pop();
    }
  }


// Methods to Handle Peer Connections and Handshakes

// Create a Peer Connection (On 'Ready')
let createPeerConnection = (pc_idx) => {
    try{
        peercons[pc_idx] = new RTCPeerConnection(PEER_CONNECTION_CONFIG); // Create a peer connection
        peercons[pc_idx].onicecandidate = onIceCandidate(pc_idx);  // called when remote sends us ICE candidate
        peercons[pc_idx].ontrack = onAddTrack(pc_idx);        // called after remote adds media track to peer connection
        localStream.getTracks().forEach(track => peercons[pc_idx].addTrack(track, localStream)); //Add local media tracks to Peer Connection
        console.log('Peer Connection created', pc_idx);
    }
    catch (error){
        console.error('Peer Connection failed: ', error);
    }
};


// Use the sendData event to send the ice candidate information to the signalling server
let onIceCandidate = (pc_idx) => {   // Callback wrapped with function that takes 'pc_idx' as paramter to distinguish which peer connection is sending it
    return function (event){
        if (event.candidate){
            console.log('ICE candidate', pc_idx);
            sendData({
                type: 'candidate',
                candidate: event.candidate,
                pc_idx: pc_idx
            });
        }
    }
}

// Here we can get the remote video/audio stream from the peer connection
let onAddTrack = (pc_idx) => {  // Again, callback wrapped with function with 'pc_idx' parameter to distinguish which stream to add track to.
    return function(event) {
        console.log("Add Stream", pc_idx);

        // When remote adds their local stream, display this in our local browser
        remoteStreams[pc_idx] = event.streams[0];

        // Dynamically create HTML5 video element
        let videoelem = document.getElementById("remotestream" + String(pc_idx));
        if (videoelem == null){
            let video = document.createElement('video');
            video.autoplay = true;
            video.setAttribute("playsinline", '');
            video.setAttribute("id", "remotestream" + String(pc_idx));
            document.body.appendChild(video);
        }else{
            // Set the remote stream as the source for the created html element
            videoelem.srcObject = remoteStreams[pc_idx];
        }
    }   
};


// Offer (on 'Ready' and peer connection)
let sendOffer = (pc_idx) => {
    console.log('Send offer', pc_idx);
    peercons[pc_idx].createOffer().then(
        setAndSendLocalDescription(pc_idx),
        (error) => { 
            console.error('Send offer failed: ', error); 
        }
    );
};
  

// Answer to Offer
let sendAnswer = (pc_idx) => {
    console.log('Send answer', pc_idx);
    peercons[pc_idx].createAnswer().then(
        setAndSendLocalDescription(pc_idx),
        (error) => {
            console.error('Send answer failed: ', error); 
        }
    );    
};

// Local Description
let setAndSendLocalDescription = (pc_idx) => {
    return function (sessionDescription) {
        peercons[pc_idx].setLocalDescription(sessionDescription);
        console.log('Local description set', pc_idx);
        sessionDescription.pc_idx = pc_idx  // Description data will include 'pc_idx'
        sendData(sessionDescription);
    }
};


// Incoming Data - If offer comes in send answer, ICE candidate communication
let handleSignalingData = (data) => {
    console.log("Handle: ", data.type, data.pc_idx);
    let pc_idx = data.pc_idx;

    switch (data.type) {
        case 'offer':
            createPeerConnection(pc_idx);
            peercons[pc_idx].setRemoteDescription(new RTCSessionDescription(data));    
            sendAnswer(pc_idx);
            break;
        case 'answer':
            peercons[pc_idx].setRemoteDescription(new RTCSessionDescription(data));
            break;
        case 'candidate':
            peercons[pc_idx].addIceCandidate(new RTCIceCandidate(data.candidate));
            break;
    }
};


// Run the main function to get local stream and start peer connection
getLocalStream();
